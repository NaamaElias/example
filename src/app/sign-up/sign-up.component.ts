import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'signup',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;

  onSubmit(){
    this.authService.signUp(this.email, this.password).then(
      res =>{
      console.log('Succesful login');
      this.router.navigate(['/welcome']);
       }
    ).catch(
      err => {
        console.log(err);
        this.isError = true;
        this.errorMessage = err.message;
      }
    )
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}