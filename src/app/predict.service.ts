import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://57ucf7dl6d.execute-api.us-east-1.amazonaws.com/beta";


  predict(education:number, income:number){
    let json = {
      "data": {
          "education": education,
          "income": income
        }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body; 
      })
    )
  }

  constructor(private http:HttpClient) { }
}
