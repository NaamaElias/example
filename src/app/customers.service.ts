import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection}from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  // public getCustomers(userId){
  //     this.customerCollection = this.db.collection(`users/${userId}/customers`); 
  //     return this.customerCollection.snapshotChanges().pipe(map(
  //       collection =>collection.map(
  //         document => {
  //           const data = document.payload.doc.data(); 
  //           data.id = document.payload.doc.id;
  //           return data; 
  //         }
  //       )
  //     ))
  // }

  getCustomers(userId): Observable<any[]> {
    this.customerCollection = this.db.collection(`users/${userId}/customers`) 
      //  ref => ref.limit(10))
    return this.customerCollection.snapshotChanges();    
  }

  addCustomer(userId:string, name:string, education:number, income:number){
    const customer = {name:name, education:education, income:income};
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  public deteleCustomer(Userid:string ,id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete();
  }

  updateCustomer(userId:string,id:string, name:string, education:number, income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        income:income,
        result:null
      }
    )
  }

  updateResult(userId:string, id:string, result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      }
    )
  }


  constructor(private db:AngularFirestore) { }
}
