import { Customer } from './../interfaces/customer';
import { PredictService } from './../predict.service';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  userId:string;
  customers$;
  customers:Customer[];
  addCustomerFormOpen = false;
  editstate = [];
  result:string;
  rowToEdit:number = -1;
  customerToEdit:Customer = {name:null, education:null, income:null};

  
 

  displayedColumns: string[] = ['name', 'education', 'income', 'delete', 'edit','predict','result'];
  
  constructor(private customersService:CustomersService, public authService:AuthService, private predictService:PredictService) { }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.education, customer.income);
  }

  deleteCustomer(id:string){
    this.customersService.deteleCustomer(this.userId,id);
  }

  // update(customer:Customer){
  //   this.customersService.updateCustomer(this.userId,customer.id, customer.name, customer.education, customer.income);
  //   }

  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }

  update(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
    this.rowToEdit = null;
  }

  sendData(i, education:number, income:number){
    this.predictService.predict(education, income).subscribe(
      res => {console.log(res);
      this.result = res;
      if(res > 0.5){
          var message = 'Will pay';
        } else {
          var message = 'Will not pay'
        }
        this.customers[i].result = message}  
    );
  }

  savePredict(i, id, result){
    this.customersService.updateResult(this.userId,id, result);
    this.customers[i].saved = true;
  }
  

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              for (let document of docs) {
                const customer:Customer = document.payload.doc.data();
                if (customer.result){
                  customer.saved = true;
                }
                customer.id = document.payload.doc.id;
                this.customers.push(customer); 
              }                        
            }
          )
      })
 }   

}
