import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  isError:boolean = false;
  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, education:this.education, income:this.income};
    if(this.education<0 || this.education>24){
      this.isError = true;
    }else{
      this.update.emit(customer);
      if(this.formType == "Add customer"){
        this.name = null;
        this.education = null;
        this.income = null;}
        this.closeEdit.emit();
      }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
